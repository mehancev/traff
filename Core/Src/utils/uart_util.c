/*
 * uart_util.c
 *
 *  Created on: May 1, 2020
 *      Author: ssdmt@mail.ru
 */

#include "main.h"
#include "uart_util.h"

static struct uart_obj_s uart[UART_DEV_INSTANCES] __attribute__ ((aligned(4))) = {0};

static uint8_t uart_rx[5] = {0};

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	uint8_t index, c;

	c = uart_rx[0];
	index =  (huart->Instance == UART_JETSON) ? UART_DEV_JETSON : UART_DEV_GPS;
	uart[index].buf[uart[index].tail] = c;

	uart[index].tail++;
	if (uart[index].tail >= UART_BUF_SIZE)		// движение по кругу
		uart[index].tail = 0;
	if (uart[index].tail == uart[index].head) {
		uart[index].head = uart[index].tail+1; // tail двигает head'впереди'cебя
		if(uart[index].head >= UART_BUF_SIZE)	// движение по кругу
			uart[index].head = 0;
	}

	HAL_UART_Receive_IT(huart, uart_rx, 1); // loop interrupt
}

uint8_t uart_char(uart_device_t index)
{
	uint8_t res;

	if (uart[index].head == uart[index].tail) // ещё ничего не пришло из uart
		return 0;

	// loc
	res = uart[index].buf[uart[index].head];
	uart[index].head++;
	if (uart[index].head >= UART_BUF_SIZE)
			uart[index].head = 0;
	// unloc

	return res;
}


/////////////////// user api ///////////////////////////////////////


void uart_start(UART_HandleTypeDef *huart)
{
	// for start interrupt only
	HAL_UART_Receive_IT(huart, uart_rx, 1);
}

void uart_stop(UART_HandleTypeDef *huart)
{
	HAL_UART_Abort(huart);
}

void uart_ack(UART_HandleTypeDef *huart)
{
	HAL_UART_Transmit(huart, "OK\r\n", 4, 100);
}

void uart_buffer_clear(UART_HandleTypeDef *huart)
{
	uint8_t index =
			(huart->Instance == UART_JETSON) ? UART_DEV_JETSON : UART_DEV_GPS;
	uart[index].head = 0;
	uart[index].tail = 0;
}
