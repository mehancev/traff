/*
 * uart_util.h
 *
 *  Created on: May 1, 2020
 *      Author: ssdmt@mail.ru
 */

#ifndef SRC_UTILS_UART_UTIL_H_
#define SRC_UTILS_UART_UTIL_H_

#define UART_GPS USART1
#define UART_JETSON USART2

#define UART_BUF_SIZE		200

typedef enum uart_device {
	UART_DEV_GPS,
	UART_DEV_JETSON,
	UART_DEV_INSTANCES // total
} uart_device_t;

struct uart_obj_s
{
	uint16_t head;
	uint16_t tail;
	char buf[UART_BUF_SIZE];		// 1024 byte
};

enum uart_cmd_e {
	  UART_CMD_IR = 1,
	  UART_CMD_IRIS,
	  UART_CMD_FOCUS,
	  UART_CMD_GPS,
	  UART_CMD_GSENS,
	  UART_CMD_TEMP,
	  UART_CMD_REBOOT,
	  UART_CMD_FAN,
	  UART_CMD_HEAT
};


uint8_t uart_char(uart_device_t dev_index);

void uart_start(UART_HandleTypeDef *huart);
void uart_stop(UART_HandleTypeDef *huart);
void uart_ack(UART_HandleTypeDef *huart);
void uart_buffer_clear(UART_HandleTypeDef *huart);

void LOG(uint8_t *text);

#endif /* SRC_UTILS_UART_UTIL_H_ */
