/*
 * app_status.h
 *
 *  Created on: May 1, 2020
 *      Author: ssdmt
 */

#ifndef SRC_UTILS_APP_STATUS_H_
#define SRC_UTILS_APP_STATUS_H_

#define LOGG 1

//typedef struct app_s {
//	int id;
//	int tier;
//	int parent;
//	int previous;
//	int child;
//	u32 gflags;
//	u32 flags;
//	int (*start)(void);
//	int (*stop)(void);
//	int (*on_message)(u32 msg, u32 param1, u32 param2);
//} app_t;


//typedef struct rec_car_video_s {
//#if defined(CONFIG_AMBA_STREAMING)
//	u8 rec_curr_mode;
//	u8 rec_next_mode;
//#define REC_CAP_MODE_STOP		(0x00)
//#define REC_CAP_MODE_VF			(0x01)
//#define REC_CAP_MODE_RECORD		(0x02)
//#define REC_CAP_MODE_CAPTURE		(0x03)
//#define REC_CAP_MODE_FWUPDATE	(0x04)
//#define REC_CAP_MODE_PB			(0x05)	//represent that camera is in thumb/pb mode
//#endif
//	u8 rec_cap_state;
//#define REC_CAP_STATE_PREVIEW		(0x00)
//#define REC_CAP_STATE_RECORD		(0x01)
//#define REC_CAP_STATE_PRE_RECORD	(0x02)
//#define REC_CAP_STATE_FOCUS		(0x03)
//#define REC_CAP_STATE_CAPTURE		(0x04)
//#define REC_CAP_STATE_FFLUSH		(0x05)
//#define REC_STATE_VIDEO_SIZE_CAPTURE	(0x06)
//#define REC_CAP_STATE_RESET		(0xFF)
//	u8 qv_file_type;
//	u8 enc_rotate;
//	u8 popup_type;
//#define POPUP_QMENU		(0x00)
//	u8 qmenu_cur;
//#if defined (ENABLE_CARDV)
//#define QMENU_SELFTIMER			(0)
//#define QMENU_REC_MODE			(1)
//#define QMENU_CAP_MODE			(2)
//#define QMENU_UP_NUM			(3)
//#define QMENU_VIDEO_RES		(3)
//#define QMENU_VIDEO_QUALITY		(4)
//#define QMENU_PHOTO_SIZE		(5)
//#define QMENU_PHOTO_QUALITY		(6)
//#define QMENU_NUM				(7)
//#define QMENU_CLOSE				(0xFF)
//#endif
//	int rec_time;
//	int self_timer_cd;
//	u8 self_timer_type;
//#define SELF_TIMER_TYPE_PHOTO	(0)
//#define SELF_TIMER_TYPE_VIDEO	(1)
//	u32 tv_jack_msg;
//	int time_lapse_timer_cd;
//	int time_lapse_counter;
//	int rec_fchan_vout_mode;
//	int (*func)(u32 func_id, u32 param1, u32 param2);
//	int (*gui)(u32 gui_cmd, u32 param1, u32 param2);
//	rec_car_video_op_t *op;
//	u8 parking_mode;
//#define PARK_MODE_OFF	(0)
//#define PARK_MODE_ON	(1)
//	struct event_s {
//		u8 recording_on;
//#define EVENT_RECORDING_OFF			(0)
//#define EVENT_RECORDING_SPLIT_MAIN	(1)
//#define EVENT_RECORDING_SPLIT_EVENT	(2)
//#define EVENT_RECORDING_SPLIT_PARK_1	(3)	// For parking mode
//#define EVENT_RECORDING_SPLIT_PARK_2	(4)	// For parking mode
//#define EVENT_RECORDING_WAIT_STOP	(5)
//#define EVENT_RECORDING_WAIT_STOP2	(6)
//		u8 nand_saving_on;
//#define EVENT_NAND_SAVING_OFF				(0)
//#define EVENT_NAND_SAVING_SPLIT_MAIN		(1)
//#define EVENT_NAND_SAVING_EVENT			(2)
//#define EVENT_NAND_SAVING_WAIT_STOP		(3)
//#define EVENT_NAND_SAVING_WAIT_STOP2	(4)
//		u8 mode_change;
//#define EVENT_MODE_RESET	(0)
//#define EVENT_MODE_CHANGING	(1)
//		int mark_start;
//		int mark_ref;
//		int extra_time;
//		// Event record: Block event during EVENT_RECORDING_WAIT_STOP until HMSG_MMGR_MUXER_OP_COMPLETE received.
//		u8 block_event_op; // 0:no block event ; 1:block event process (F4 to split main) ; 2: block event process (split main to wait stop)
//		// Event record: Block event during EVENT_RECORDING_WAIT_STOP2 until HMSG_MMGR_MUXER_OP_COMPLETE received(Only for 2CH case).
//		u8 block_event_op2;
//		// Event record: Block emergency event(overlapped) until HMSG_MMGR_MUXER_OP_COMPLETE received.
//		u8 block_emergency_op; // 0: no emergency event ; 1: emergency event processing (F5 to split main) ; 2: emergency event process (split main to wait stop)
//		// Event record: Block emergency event(non-overlapped) until HMSG_MMGR_MUXER_OP_COMPLETE received.
//		u8 block_emergency_op2; // 0: no emergency event ; 1: emergency event processing
//		// count for op_completed
//		u8 count_for_op;
//		u8 count_for_stop;
//		u8 count_for_main;
//		u8 count_for_park;
//		u8 count_for_dual_saving;
//		u8 event_folder_full;
//		u8 emergency_folder_full;
//	} event;
//	int lock_files;
//#define LOCK_FILE_INIT_TWO_FILES	(-2)
//#define LOCK_FILE_INIT			(-1)
//#define LOCK_FILE_RESET			(0)
//#define LOCK_TWO_FILES			(2)
//#define LOCK_THREE_FILES		(3)
//	int mic_mute;
//#define MIC_UNMUTE				(0)
//#define MIC_MUTE				(1)
//	u64 storage_volume;
//	int delayed_bt4;
//#define DELAYED_REC_CAR_VIDEO_EMERGENCY	(1)
//#define DELAYED_REC_CAR_VIDEO_EDTMGR	(2)
//	int async_op_count;
//	int split_interval;
//#define EMERGENCY_INTERVAL	(120)	// 2 minutes
//	struct edit_s {
//		u8 flow;
//#define EDIT_FLOW_2_FILES	(0)
//#define EDIT_FLOW_3_FILES	(1)
//		u8 type;
//		u8 state;
//#define EDIT_STATE_NONE	(0)
//#define EDIT_STATE_WAITING_SPLIT_1ST	(1)
//#define EDIT_STATE_CROP2NEW_1ST 	(2)
//#define EDIT_STATE_CROP2NEW_1ST_1	(3)
//#define EDIT_STATE_CROP2NEW_1ST_2	(4)
//#define EDIT_STATE_WAITING_SPLIT_2ND	(5)
//#define EDIT_STATE_CROP2NEW_2ND	(6)
//#define EDIT_STATE_MERGE_1ST	(7)
//#define EDIT_STATE_MERGE_2ND	(8)
//		u8 src_flags;
//#define EDIT_SRC_FLAGS_SPLIT_1ST_READY	(0x01)
//#define EDIT_SRC_FLAGS_SPLIT_2ND_READY	(0x02)
//		u16 io_rate;
//		u16 count_to_split;
//		int mark_start;
//		int mark_ref;
//		int split_duration_1st;
//		MCHAR fn_tmp1[APP_MAX_FN_SIZE];
//		MCHAR fn_tmp2[APP_MAX_FN_SIZE];
//		MCHAR fn_tmp3[APP_MAX_FN_SIZE];
//	} edit;
//	u8 fov_count;
//#if defined(CONFIG_SUPPORT_BOSS)
//	int ref_mqid;
//	int clntid;
//	int error_type;
//#define REC_ERR_STORAGE_RUNOUT	1
//#define REC_ERR_STORAGE_IO_ERROR	2
//#define REC_ERR_MEMORY_RUNOUT	3
//#define REC_ERR_REACH_INDEX_LIMIT	4
//#define REC_ERR_REACH_FILE_LIMIT	5
//#define REC_ERR_CARD_REMOVED	6
//#define REC_ERR_CARD_PROTECTED 7
//#define REC_ERR_CANNOT_ISSUE_PIV 8
//#endif
//	u32 rtc_timer_tick;
//	u32 timer_sync_check;
//// parking mode begin
//	u32 park_mode;
//#define PARK_OFF (0)
//#define PARK_ON	(1)
//	u32 park_mode_auto_power_counter;
//	u32 park_mode_flag;
//#define PARK_MODE_SWITCH_TO_VIDEO (0x01)
//#define PARK_MODE_BUTTON_IS_PRESSED (0x02)
//#define PARK_MODE_HIGHEST_GSENSOR_SENSE (0x04)
//#define PARK_MODE_HIGHEST_UPDATE_POSITION (0x08)
//	u32 cyclic_rewriting_flag;
//#define CYCLIC_REWRITING_STORAGE_RUNOUT (0x01)
//#define CYCLIC_REWRITING_SECOND_LOOP (0x02)
//#define CYCLIC_REWRITING_SAVE_LAST_FILE (0x04)
//#define CYCLIC_REWRITING_RECORD_START_SWITCH (0x08)
//	u32 aexp_flag;
//#define AEXP_DAY (0x01)
//	u32 speed_in_move_flag;
//#define SPEED_IN_MOVE_ON (0x01)
//#define SPEED_IN_MOVE_SPEED (0x02)
//#define SPEED_IN_MOVE_2WIDTH (0x04)
//#define SPEED_IN_MOVE_3WIDTH (0x08)
//// parking mode end
//} rec_car_video_t;

#endif /* SRC_UTILS_APP_STATUS_H_ */
