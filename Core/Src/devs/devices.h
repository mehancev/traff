/*
 * devices.h
 *
 *  Created on: May 4, 2020
 *      Author: ssdmt@mail.ru
 */

#ifndef SRC_DEVS_DEVICES_H_
#define SRC_DEVS_DEVICES_H_

enum ir {
	IR_POWER = 1
};
enum iris {
	IRIS_OPEN = 1,   // 1 step
	IRIS_CLOSE,      // 1 step
	IRIS_CALIBRATE
};
enum focus {
	FOCUS_LEFT = 1,   // 1 step
	FOCUS_RIGHT,       // 1 step
	FOCUS_CALIBRATE
};
enum gps {
	GPS_COORDS = 1,
	GPS_TIMESTAMP
};
enum fan {
	FAN_ON,
	FAN_OFF
};
enum heat {
	HEAT_ON,
	HEAT_OFF
};

void ir_power(void);
void lens_iris(void);
void lens_focus(void);
void gps(void);


#endif /* SRC_DEVS_DEVICES_H_ */
