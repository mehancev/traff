/*
 * gps.c
 *
 *  Created on: May 4, 2020
 *      Author: ssdmt@mail.ru
 */
#include <string.h>

#include "main.h"
#include "../utils/uart_util.h"
#include "devices.h"

extern UART_HandleTypeDef huart_jetson;
extern UART_HandleTypeDef huart_gps;

static uint8_t gps_data[100];
//static uint8_t gps_data[100]="$GNRMC,060633.000,A,5647.787156,N,06030.703488,E,0.04,51.15,100620,,,A*\0";
static uint8_t lat[13] = {'0','0','0','0','.','0','0','0','0','0','0','\r','\n'};
static uint8_t lng[14] = {'0','0','0','0','0','.','0','0','0','0','0','0','\r','\n'};
static uint8_t date[9] = {'2','0','0','6','1','0','\r','\n','\0'};
static uint8_t time[9] = {'1','4','5','0','2','1','\r','\n','\0'};

static void get_gps_coords(uint8_t *gps, uint8_t *_lat, uint8_t *_lng);
static void get_gps_timestamp(uint8_t *gps,  uint8_t *_date, uint8_t *_time);

void gps(void)
{
	uint8_t cmd,s,i=0;

	cmd = uart_char(UART_DEV_JETSON);

	uart_stop(&huart_jetson);
	uart_buffer_clear(&huart_gps);
	uart_start(&huart_gps);

	HAL_Delay(2000);

	while(s!='$') {
		s = uart_char(UART_DEV_GPS); HAL_Delay(1);
	}
	while(s!='*') {
		s = uart_char(UART_DEV_GPS); HAL_Delay(1);
		if (s == '*') break;
		if (s==0) continue;
		gps_data[i++] = s;
	}
	gps_data[i] = 0;
	uart_stop(&huart_gps);

	if (cmd == GPS_COORDS)
	{
		get_gps_coords(gps_data, lat, lng);

		HAL_UART_Transmit(&huart_jetson, lat, 13, 100);
		HAL_UART_Transmit(&huart_jetson, lng, 14, 100);
	}
	else if (cmd == GPS_TIMESTAMP)
	{
		get_gps_timestamp(gps_data, date, time);

		HAL_UART_Transmit(&huart_jetson, date, 8, 100);
		HAL_UART_Transmit(&huart_jetson, time, 8, 100);
	}

	uart_ack(&huart_jetson);
	uart_buffer_clear(&huart_jetson);
	uart_start(&huart_jetson);
}

static
void get_gps_coords(uint8_t *gps, uint8_t *_lat, uint8_t *_lng)
{
	uint8_t i=0, cnt_comma=0, cnt_lat=0, cnt_lon=0;

	while(i < strlen(gps))
	{
		if(gps[i]==',')
		{
			cnt_comma++;
			i++;

			if((cnt_comma == 2) && (gps[i] == 'V')) //+
				return; // lat=0.0; lon=0.0

			if(cnt_comma==3) { // LAT
				while (gps[i] != ',')
					_lat[cnt_lat++] = gps[i++];
				i--;
			}

			if(cnt_comma==5) { // LON
				while (gps[i] != ',')
					_lng[cnt_lon++] = gps[i++];
				break;
			}
		}
		i++;
	}
	return;
}

static
void get_gps_timestamp(uint8_t *gps,  uint8_t *_date, uint8_t *_time)
{
	uint8_t i=0, cnt_comma=0, cnt_date=0, cnt_time=0;

	while(i < strlen(gps)) {
		if(gps[i]==',') {
			cnt_comma++;
			if(cnt_comma == 2) {
				if(gps[i+1] == 'V')
					return; // time, data default
				else
					break;
			}
		}
		i++;
	}

	i=0;
	cnt_comma=0;
	while (i < strlen(gps))
	{
		if (gps[i]==',') {
			cnt_comma++;
			i++;
			if (cnt_comma==1) { //time
				while (gps[i] != '.') {
					_time[cnt_time++] = gps[i++];
				}
				i=i+2;
			}
			if (cnt_comma==9) { //data
				while (gps[i] != ',') {
					_date[cnt_date++] = gps[i++];
				}
				break;
			}
		}
		i++;
	}
}
