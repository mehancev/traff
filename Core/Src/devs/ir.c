/*
 * ir.c
 *
 *  Created on: May 4, 2020
 *      Author: ssdmt@mail.ru
 */
#include <string.h>

#include "main.h"
#include "../utils/uart_util.h"
#include "devices.h"


extern UART_HandleTypeDef huart_jetson;

void ir_power(void)
{
	// arg = [0..100]  - percent of power IR light
	uint8_t cmd, power;

	cmd = uart_char(UART_DEV_JETSON);

	if (cmd != IR_POWER)
		return;

	power = uart_char(UART_DEV_JETSON); // 0..100%

	// set IR power
	// настройки установили ранее в main
	// здесь же просто меняем настройки pwm

	HAL_UART_Transmit(&huart_jetson, &power, 1, 50); // DBG
	uart_ack(&huart_jetson);
}
