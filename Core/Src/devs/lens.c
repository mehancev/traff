/*
 * lens.c
 *
 *  Created on: May 4, 2020
 *      Author: ssdmt@mail.ru
 */

#include <string.h>

#include "main.h"
#include "../utils/uart_util.h"
#include "devices.h"

extern UART_HandleTypeDef huart_jetson; // jetson

void lens_iris(void)
{
	uint8_t cmd;

	cmd = uart_char(UART_DEV_JETSON);

	if (cmd == IRIS_OPEN) {
		// TODO
	}

	if (cmd == IRIS_CLOSE) {
		// TODO
	}

	if (cmd == IRIS_CALIBRATE) {
		// TODO
	}

	HAL_UART_Transmit(&huart_jetson, &cmd, 1, 50);
	uart_ack(&huart_jetson);
}

void lens_focus(void)
{
	uint8_t cmd;

	cmd = uart_char(UART_DEV_JETSON);

	if (cmd == FOCUS_LEFT) {
		// TODO
	}

	if (cmd == FOCUS_RIGHT) {
		// TODO
	}

	if (cmd == FOCUS_CALIBRATE) {
		// TODO
	}

	HAL_UART_Transmit(&huart_jetson, &cmd, 1, 50);
	uart_ack(&huart_jetson);
}
